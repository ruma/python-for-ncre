Python语言基本语法元素
=====
程序的格式框架
-----
缩进：Tab键或4个空格（建议）。用于表示程序间的包含和层次关系。例如if、while、for、def、class等保留字使用缩进。unexpected indent表示缩进错误。
```python
d={}
for c in(65,97):
    for i in range(26):
        d[chr(i+c)]=chr((i+13)%26+c)
print("".join([d.get(c,c) for c in d]))

```
注释：以#开头表示，多行注释每行前面都加#
```python
#这是一个真理注释
print("Python是世界上最好的语言")
```
语法元素的名称
=====
变量：保存和表示数据值，通过赋值（等号=）来修改，随时命名、随时赋值、随时使用。   

命名：给变量或其他程序元素关联名称或标识符的过程。可以采用大小写字母、数字、下划线、汉字及组合命名，长度没有限制。首字符不能是数字，标识符中间不能有空格、不能与保留字相同、大小写敏感，标点符号全部为英文标点。   

保留字：关键字，内部定义并保留使用的标识符，大小写敏感，Python3版本共33个保留字。    
```python 
#Python3保留字
and    as        assert  break   class  continue  def
def    elif      else    except  False  finally   for
from   global    if      import  in     is        lambda
None   nonlocal  not     or      pass   raise     return
True   try       while   with    yield
```

数据类型
=====
数据类型：表示数据的含义，消除计算机对数据理解的二义性。Python有数字、字符串、元祖、集合、列表、字典类型等。  

数字类型：整数、浮点数、复数。整数有十进制、十六进制、八进制、二进制，可以直接比较。浮点数带有小数点，只有十进制，可以用科学计数法表示。   

字符串类型：字符序列，用双引号" "或单引号' '包括起来（作用相同），可以对单个字符或字符片段进行索引。    

程序的语句元素
=====
表达式：产生或计算新数据值的代码片段，数据和操作符构成，运算结果的类型由操作符或运算符决定。   

赋值语句：使用等号（=）表示，变量=表达式。双等号（==）判断相等，True相等，False不相等。   
```python
>>> a = 666  #赋值
>>> print(a)
666

>>> a,b = 666,999   #同步赋值
>>> x,y = 'PHP','Python'
>>> print(x)
PHP
>>> print(a)
666

>>> x,y = y,x  #同步赋值的应用 互换变量

```
引用：(1)全面命名空间引用： import 功能库名称，调用：功能库名称.函数名称()    

           (2)具体函数引用：from 功能库名称 import 函数名，调用：函数名称()  

           (3)全函数引用：from 功能库名称 import * ，调用：函数名称()  

           (4)别名引用：import 功能库名称 as 功能库别名，调用：功能库别名.函数名称()  

```python
#引用turtle库画圆
>>> import turtle
>>> import time

```
基本输入输出函数
======
input()：变量=input("提示性文字") ，返回字符串。
```python
>>> a = input("请输入一个小数:")
请输入一个小数:6.66
>>> print(a)
6.66
```
eval()：变量=eval("字符串")，去掉字符串最外侧的引号，执行去掉引号后的字符内容。
```python
>>> a = eval("1.11 + 5.55")
>>> print(a)
6.66
 
#eval()结合input()使用 获取用户输入的数字
>>> a = eval(input("请输入一个数:"))
请输入一个数:333
>>> print(a*2)
666
```

print()：输出运算结果。
```python
#第一种 输出字符串
>>> print("挺好")
挺好

#第二种 输出一个或多个变量
>>> a=666
>>> print(a,a,a)
666 666 666

#第三种 混合输出字符串与变量值
#格式：print("字符串模板".format(变量1,2,3))  {}代表一个槽位，对应.format里的变量
>>> a,b=6,111
>>> print("数字{}和数字{}的乘积是{}".format(a,b,a*b))
数字6和数字111的乘积是666


>>> a=666   #end参数避免默认换行
>>> print(a,end=".")
666.
```






