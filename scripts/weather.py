# -*- coding: utf-8 -*-
import urllib2,json
import smtplib
from email.mime.text import MIMEText

Marui = ['CANTON','HENGSHUI','WUHAN']
Qianwen = ['CHANGCHUN','WUHAN']

userlist = {'Marui':Marui,'Qianwen':Qianwen}

mails = {'Qianwen' : '****@qq.com',
         'Marui' : '****@outlook.com',
        }

city = {
        '广州': 'CN101280101',
        'CANTON' : 'CN101280101',
        'GUANGZHOU' : 'CN101280101',
        '长春' : 'CN101060101',
        'CHANGCHUN' : 'CN101060101',
        '衡水' : 'CN101090801',
        'HENGSHUI' : 'CN101090801',
        '张家口' : 'CN101090301',
        'ZHANGJIAKOU' : 'CN101090301',
        'XINYANG' : 'CN101180601',
        '武汉' : 'CN101200101',
        'WUHAN' : 'CN101200101',
        }

def getweather(url):
    req = urllib2.Request(url)
    resp = urllib2.urlopen(req).read()
    json_data = json.loads(resp)
    city_data=json_data['HeWeather5'][0]
    daily_data = json_data['HeWeather5'][0]['daily_forecast']
    weather = u"今日日期: {0}, 城市: {1}, 空气质量: {2}, PM2.5: {3}, 白天天气: {4}, 夜间天气: {5}\n今日温度: {6}°C / {7}°C, 风向: {16}, 风速: {17}级 \n明日温度: {8}°C / {9}°C, 风向: {18}, 风速: {19}级 \n后日温度: {10}°C / {11}°C, 风向: {20}, 风速: {21}级 \n空气质量: {12}\n舒适程度: {13}\n穿衣建议: {14}\n紫外线强度: {15}\n".format(daily_data[0]['date'],city_data['basic']['city'],city_data['aqi']['city']['qlty'],city_data['aqi']['city']['pm25'],daily_data[0]['cond']['txt_d'],daily_data[0]['cond']['txt_n'],daily_data[0]['tmp']['min'],daily_data[0]['tmp']['max'],daily_data[1]['tmp']['min'],daily_data[1]['tmp']['max'],daily_data[1]['tmp']['min'],daily_data[2]['tmp']['max'],json_data['HeWeather5'][0]['suggestion']['air']['txt'],json_data['HeWeather5'][0]['suggestion']['comf']['txt'],json_data['HeWeather5'][0]['suggestion']['drsg']['txt'],json_data['HeWeather5'][0]['suggestion']['uv']['txt'],daily_data[0]['wind']['dir'],daily_data[0]['wind']['sc'],daily_data[1]['wind']['dir'],daily_data[1]['wind']['sc'],daily_data[2]['wind']['dir'],daily_data[2]['wind']['sc'])
    return weather

def usermail(users):
    msm = ""
    for i in users:
        #print i
        url = 'https://free-api.heweather.com/v5/weather?city={0}&key=8a439a7e0e034cdcb4122c918f55e5f3'.format(city[i])
        weather = getweather(url)
        msm = msm+"\n"+weather
    return msm

def sendmail(user,msm):
    sender = '********'
    passwd = '********'
    receiver = [mails[user]]
    content = "Dear "+user+" :\n"+msm+"\n \n  Yours\n  Rui"
    subject = '您所订阅的天气如下'
    msg = MIMEText(content, 'plain', 'utf-8')  
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = ','.join(receiver) 
    server = smtplib.SMTP_SSL("smtp.qq.com", 465)
    server.login(sender,passwd)
    try:
        server.sendmail(sender, receiver, msg.as_string())
        print('E-mail send to {0} completed.'.format(user))
    except smtplib.SMTPException as e:
        print(e)

for user in userlist:
    msm = usermail(userlist[user])
    sendmail(user,msm)




