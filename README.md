安装Python及计算机二级所需程序库
=====
下载并安装Python
-----

计算机二级软件包[点击下载](http://ncre.neea.edu.cn/res/Home/1808/31bbb56a542229f36d1f4a594f9147a6.rar)  
软件包安装说明[点击下载](http://ncre.neea.edu.cn/res/Home/1808/3113da61a464fcf9e4cbbcaed6127a81.docx)  

Python软件安装步骤
-----
```
1.	选择python-3.5.3.exe安装文件，双击进行安装。  
2.	勾选“Add Pyhon 3.5 to PATH”，选择“Customize installation”。   
3.	点击“Install Now”  
4.	安装完成后进入系统命令行，输入python --version，正确显示版本号说明安装成功。  
5.	安装第三方库(jieba库和PyInstaller)。执行InstallScript.pyi（双击打开即可自动安装） 注意：有网络时，脚本会联网安装；如果没有网络，请在脚本所在目录下确保放置future-0.16.0.tar.gz、pefile-2018.8.8.tar.gz、jieba-0.39.zip和PyInstaller-3.3.1.tar.gz等文件。  
6.	如果系统实在无法安装python-3.5.3.exe文件，请安装Python 3.4.3版本：执行python-3.4.3.msi文件。  
```





